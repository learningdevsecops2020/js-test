#!/bin/bash
for j in `cat gljs.txt`;do
#echo Checking $j javascript for sensitive information and leaks;
echo checking $j >> lf.txt;
#echo Secretfinder check:
#echo "======================================"
#python3 ~/Yogesh/tools/SecretFinder/SecretFinder.py -i $j -o cli
#echo "======================================"
#echo Linkfinder check:
#echo "======================================"
python3 ~/Yogesh/tools/LinkFinder/backuplf.py -i $j -o cli >> lf.txt
#python3 ~/Yogesh/tools/SecretFinder/backup.py -i $j -o cli >> sf.txt
done
sed '/checking/{$d;N;/\n.*checking/D;}' lf.txt >> sorted.txt; #We are filtering the js results only with js files which has output
cat sorted.txt | awk '/checking/{print "==========================================================================="}1' | awk '/checking/{print;print "===========================================================================";next}1' >> qa.txt #For Optimised result we are using this command
cupsfilter qa.txt > output-lf.pdf #producing result in pdf format.
cp output-lf.pdf /Users/yogeshwaran/Yogesh/
#sed '/checking/{$d;N;/\n.*checking/D;}' sf.txt >> outputone.txt
#cat outputone.txt | awk '/checking/{print "==========================================================================="}1'| awk '/checking/{print;print "===========================================================================";next}1' >> outputtwo.txt
#cupsfilter outputtwo.txt > output-sf.pdf

